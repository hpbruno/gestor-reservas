/*
 * @Author: Bruno Henrique
 */ 

module.exports = app => {

    // Rotas de entrada...
    const addReserva = async (req, res) => {
       
        // Body da requisicao..
        let reserva = req.body 

        // Vamos validar os dados que chegaram 
        if (validarReserva( reserva )) {
            await cadastrarReserva( reserva )
            res.status(201).send({  msg: "Reserva cadastrada com sucesso!", type: "ok"  })
        }
       
    }


    // Validar dados de cadastro 
    const validarReserva = ( reserva ) => {

        // auxiliares  
        let ehVazio = app.config.validation.ehVazio

        if (ehVazio( reserva.TITULO )) {
            return { msg: "Informe um titulo para a reserva!", type: "err" }
        } 

        console.log('data '+ ehVazio(reserva.DATEINICIO));
        if (ehVazio( reserva.DATEINICIO )) {
            return { msg:'Favor informe uma data para o inicio', type: 'err'};
        }           

        if (ehVazio( reserva.DATEFIM )) {
            return { msg:'Favor informe uma data para o final', type: 'err'};
        }  

        // validar data         
        validaData = validarData(reserva.DATEINICIO, reserva.DATEFIM )        
        if ( validaData != {} ) return validaData

        return {}
    }

    const validarData = ( dateini, datefim ) => {

        // Convertendo para segundos
        datenow = Math.floor(new Date() / 1000 ) 
        dateini = Math.floor(new Date( dateini ) / 1000 )
        datefim = Math.floor(new Date( datefim ) / 1000 )

        if ( ( dateini < datenow ) || (datefim < datenow) ){
            return { msg:'Revise os campos de data, elas deve ser maiores que a data de hoje.', type: 'err'};
        }

        if ( dateini > datefim ){
            return { msg:'Revise os campos de data, a data final nao pode ser melhor que a data incial.', type: 'err'};
        }

        // ** validar aqui se nao entra em conflito com outros horarios.  **//
        
    }

    const updateReservas = async ( req, res ) => {
        
        let isValid

        // connnection  
        const con = app.config.db.connection

        // SQL update 
        const functionSql = app.config.dic.gerarUpdate

        // Data 
        let reservas = req.body

        isValid = validarReserva( reservas )                
        
        if ( !isValid ) {

            try {

                // vamos receber o update 
                let sql = functionSql('reservas', 'ID = ' + reservas.ID ) 

                con.query(sql , [ reservas.TITULO, reservas.IDCLIENTE, reservas.DATEINICIO, reservas.DATEFIM, reservas.CORTAG ], (err, result, fields) => {

                    if (err) res.status(400).send('Erro: ' + err + '\n' + 'SQL:' + sql)

                    res.status(200).send( {msg: 'Atualizado com sucesso!' , type: 'ok'})

                })

            } catch ( err ) {

                res.status(400).send('Erro Servidor: ' + err )

            }
        } else { 
            res.status(201).send( isValid )
        }

    }
     

    const cadastrarReserva = async ( reserva ) => {

        // Responsavel por preprarar o insert... 
        const functionSql = app.config.dic.gerarInsert

        // Connection
        const con = app.config.db.connection 

        // sql 
        const sql = await functionSql('reservas')

        // values 
        let values = [ reserva.TITULO, reserva.IDCLIENTE, reserva.DATEINICIO, reserva.DATEFIM, reserva.CORTAG ]

        con.query(sql, [ values ], ( err, res, fields ) => {
            if (err) console.log('Erro ao registrar reserva. '+err);            
        })

    }

    const retornarReservas = async (req, res) => {

        let where

        // Recuperar instancia da funcao
        const fncSelect = app.config.dic.gerarSelect 

        // verificar se iremos aplicar os filtros 
        if ( (req.body.datainicial) && (req.body.datafinal) ) {
            
            let datainicial = formatarData('yyyymmdd hhmm', req.body.datainicial )
            let datafinal   = formatarData('yyyymmdd hhmm', req.body.datafinal )

            where = 'DATAINICIO >= "' + datainicial + '" AND ' + 
                    'DATAFIM    <= "' + datafinal +' " '
        } else {
            where = ''
        } 

       // Se tiver id, vamos retornar conforme o mesmo. 
       let  sql = fncSelect ('reservas', where, 'DATAINICIO')        

       console.log(sql);
       // Connection
       const con = app.config.db.connection 

       con.query(sql, [], (err, result, fields) => {
           if (err) console.log(err);                    

            console.log(result);

           res.status(200).send(result)
       })

    }



    const  formatarData = (format, date) => {

        // formatar data para segundos | REVER : a data aqui precisa chegar em milisegundos 
        let secDate = new Date(date)

        // Ajustar formato mes
        let mes = (secDate.getMonth() +1)
        if (mes < 10) mes = '0'+mes 

        // Ajustar formato dia 
        let dia = secDate.getUTCDate()
        if (dia < 10) dia = '0'+dia

        // date 
        if (format === 'dd/mm/yyyy') {
            //     // Dia do mês...             // retorna:(0-11) + 1 temos os 12 meses   // Ano... 
            return  dia + '/' + mes       + '/' +     secDate.getFullYear()
        }

        // date and time 
        if ( format === 'dd/mm/yyyy hh:mm' ) {
            return dia + '/' + mes + '/' + secDate.getFullYear() + ' ' + secDate.getHours()+':'+secDate.getMinutes()
        }

        // DAte and time conforme inputs        
        if ( format === 'yyyy-mm-dd hh:mm' ) {
            return secDate.getFullYear() + '-' + mes + '-' + dia + 'T' + secDate.getHours()+':'+secDate.getMinutes()
        }

        // Date para comparacao no banco 
        if ( format === 'yyyymmdd hhmm' ) {
            return secDate.getFullYear() + '-' + mes + '-' + dia + ' ' + secDate.getHours()+':'+secDate.getMinutes()
        }
    }





    const retornarReserva = async (req, res) => {

        // Recuperar instancia da funcao
        const fncSelect = app.config.dic.gerarSelect 

        // Se tiver id, vamos retornar conforme o mesmo. 
        let sql = fncSelect('reservas', 'ID = (?)', '')        

        // Connection
        const con = app.config.db.connection 

        con.query(sql, [ req.body.ID ], (err, result, fields) => {
            if (err) console.log(err);                    
            res.status(200).send(result)
        })

    }

    return { addReserva, retornarReservas, retornarReserva, updateReservas }

}