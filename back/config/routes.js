/*
 - @Author: Bruno Henrique
*/

module.exports = app => {

    /* Rotas de entrada e validacao do usuario */
    app.route('/login')
        .post(app.api.auth.validarlogin)
    
    app.route('/register')
        .post(app.api.auth.existeLogin)

    /* Rotas de cadastro */        
    app.route('/cadastrarCliente')
        // .all(app.config.passport.authenticate())
        .post(app.api.clients.addCliente)        

    app.route('/cadastrarReserva')
        // .all(app.config.passport.authenticate())
        .post(app.api.reservas.addReserva)        

    /* Update */
    app.route('/updateCliente')
        // .all(app.config.passport.authenticate())
        .post(app.api.clients.updateCliente)
    
    app.route('/updateReservas')
        // .all(app.config.passport.authenticate())
        .post(app.api.reservas.updateReservas)

    /* Delete */
    app.route('/deleteCliente')
        // .all(app.config.passport.authenticate())
        .post(app.api.clients.deleteCliente)
        
    /* Rotas de retorno */
    app.route('/retornarClientes') 
        // .all(app.config.passport.authenticate())
        .get(app.api.clients.retornarClientes)

    app.route('/retornarCliente') 
        // .all(app.config.passport.authenticate())
        .post(app.api.clients.retornarCliente)        

    app.route('/retornarReservas') 
        // .all(app.config.passport.authenticate())
        .post(app.api.reservas.retornarReservas)

    app.route('/retornarReserva') 
        // .all(app.config.passport.authenticate())
        .post(app.api.reservas.retornarReserva)
}