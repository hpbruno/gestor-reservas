/*
 * - @Author: Bruno Henrique
 **/
const { db } = require('../.env')
const mysql = require('mysql')

// parameters
const host = db.host
const port = db.port 
const user = db.user
const password = db.password


module.exports = app => {

    // Criando connexão
    const connection = mysql.createConnection({
        host, 
        port, 
        user, 
        password
    })

    // Migrations 
    connection.connect(function(err){

        // Err 
        if (err) return console.log('Erro ao conectar ao banco. ERR: '+err)

        // Preparando Banco de dados 
        console.log('Preparando banco de dados!') 
        app.migrations.migration0001.CreateDB(connection)        

    })

    return { connection }
}