/*
 * @Author: Bruno Henrique
 */

const maskCPF = ( value ) => {

    value = value.replace(/\D/g,"")
    value = value.replace(/(\d{3})(\d)/,"$1.$2")
    value = value.replace(/(\d{3})(\d)/,"$1.$2")
    value = value.replace(/(\d{3})(\d{1,2})$/,"$1-$2")

    return value
}

const maskCNPJ = ( value ) => {

    value = value.replace(/\D/g,"")
    value = value.replace(/(\d{2})(\d)/,"$1.$2")
    value = value.replace(/(\d{3})(\d)/,"$1.$2")
    value = value.replace(/(\d{3})(\d)/,"$1/$2")
    value = value.replace(/(\d{4})(\d{1,2})$/,"$1-$2")

    // limite de carcteres 
    value = value.substring(0, 17)

    return value
}

const maskFone = ( value ) => { 
    
    value = value.replace(/\D/g,"")
    value = value.replace(/(\d{2})(\d)/,"($1) $2")    
    value = value.replace(/(\d{5})(\d)/,"$1-$2")    

    // limite de carcteres 
    value = value.substring(0, 14)

    return value
}

export { maskCPF, maskCNPJ, maskFone }