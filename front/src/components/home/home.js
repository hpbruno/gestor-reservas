// Imports 
import { maskCPF, maskCNPJ, maskFone } from '../../../mask.js'
import { ehVazio, createElements, exibirAlert, desabilitarInteracao, retornartoken,  baseapi, urlapp } from '../../../global.js';
import { TratarEventos, TratarEventosModal, procurar, SalvarCliente, CarregarDados, RetornarForm, ConfigurarViewCliente } from '../modules/clients.js'
import { ConfigurarViewReservas } from '../modules/reservas.js'


function TratarEventosView () {

    const navClientes = document.getElementById('nav-clientes') 
    navClientes.onclick = clic => {        
        ConfigurarViewCliente()        
    }

    const navReservas = document.getElementById('nav-reservas')
    navReservas.onclick = clic => {
        ConfigurarViewReservas()
    }
}


function start() {    

    // Carregar o token de acesso antes de tudo
    axios.defaults.headers.common = {'Authorization': `bearer ${retornartoken()}` }

    // Tratar aqui os enventos da tela principal
    TratarEventosView()

    // Por padrão vamos trazer na home a tela de clientes.. 
    // ConfigurarViewCliente()
    ConfigurarViewReservas()
}

start()